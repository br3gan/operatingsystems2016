#include "SkipNode.h"
#include <string.h>

// Constructors and Destructors
SkipNode::SkipNode(int searchKey, const Record *record, int level){
    m_searchKey=searchKey;
    m_record=record;
    m_level=level;
    m_next=new SkipNode*[m_level];
    for (int i=0; i<level; i++)
        m_next[i]=NULL;
}

SkipNode::SkipNode(int searchKey, const char* lastname, const char* firstname, float gpa, int numOfCourses, 
    const char* department, int postcode, int level)
{
    m_searchKey=searchKey;
    m_record = new Record(searchKey, lastname, firstname, gpa, numOfCourses, department, postcode);
    m_level=level;
    m_next=new SkipNode*[m_level];
    for (int i=0; i<level; i++)
        m_next[i]=NULL;
}

SkipNode::SkipNode(int maxLevel){
    m_searchKey=0;
    m_record=NULL;
    m_level=maxLevel;
    m_next=new SkipNode*[maxLevel];
    for (int i=0; i<maxLevel; i++)
        m_next[i]=NULL;
}

SkipNode::SkipNode(int maxLevel, int maxValue, SkipNode *head){
    m_searchKey=maxValue;
    m_record=NULL;
    m_level=maxLevel;
    m_next=new SkipNode*[maxLevel];
    for (int i=0; i<maxLevel; i++){
        m_next[i]=NULL;       
        head->m_next[i]=this;
    }
}

SkipNode::~SkipNode(){
    delete m_record;
    delete[] m_next;
}

// Setters and Getters

// class attributes
const int SkipNode::searchKey() const{
    return m_searchKey;
}
const Record* SkipNode::record() const{
    return m_record;
}    
SkipNode** SkipNode::next() const{
    return m_next;
}
const int SkipNode::level() const{
    return m_level;
}
