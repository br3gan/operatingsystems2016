#include <iostream>
#include <string.h>

struct Record{
    int studentID;
    char* lastname;
    char* firstname;
    float gpa;
    int numOfCourses;
    char* department;
    int postcode;

    Record( int studentID, const char* lastname, const char* firstname, 
            float gpa, int numOfCourses, const char* department, int postcode)
    {
        this->studentID=studentID;
        this->lastname=new char[25];
        strcpy(this->lastname,lastname);
        this->firstname=new char[25];
        strcpy(this->firstname,firstname);
        this->gpa=gpa;
        this->numOfCourses=numOfCourses;
        this->department=new char[25];
        strcpy(this->department,department);
        this->postcode=postcode;
    }
    ~Record(){
        delete[] firstname;
        delete[] lastname;
        delete[] department;
    }
};

class SkipNode{

private:
    int m_searchKey;
    const Record* m_record;
    SkipNode** m_next;
    int m_level;

public:

    /**
    * @brief Creates a SkipNode with an existing record
    *
    * @param m_searchKey The primary key of the new node
    * @param m_record Pointer to m_record attributes of the node. Expected to be allocated.
    * @param maxLevel The limit of available m_next pointers.
    */
    SkipNode(int searchKey, const Record *record, int level);

    /**
    * @brief Creates a SkipNode and allocates a record 
    *
    * @param m_searchKey The primary key of the new node
    * @param maxLevel The limit of available m_next pointers.
    */
    SkipNode(int searchKey, const char* lastname, const char* firstname, float gpa, int numOfCourses, 
        const char* department, int postcode, int level);

    /**
    * @brief Creates a Skipist head 
    *
    * @param maxLevel The limit of available m_next pointers.
    */
    SkipNode(int maxLevel);

    /**
    * @brief Creates a SkipList tail and sets Head's propper m_nexts.
    *
    * @param maxLevel The limit of available m_next pointers.
    * @param maxValue The value of the tail's m_searchKey.
    * @param head Pointer to the SkipList head.
    */
    SkipNode(int maxLevel, int maxValue, SkipNode *head);
    
    /**
    * @brief Deletes the dynamic memory of this node. 
    */
    ~SkipNode();

    // Setters and Getters
    // class attributes
    const int searchKey() const;
    const Record* record() const;
    SkipNode** next() const;
    const int level() const;

    // record attributes
    void set_studentID(int studentID);
    int studentID();

    void set_lastname(char* lastname);
    char* lastname();

    void set_firstname(char* firstname);
    char* firstname();

    void set_gpa(float gpa);
    float gpa();

    void set_numOfCourses(int numOfCourses);
    int numOfCourses();

    void set_department(char* department);
    char* department();

    void set_postcode(int postcode);
    int postcode();
 
};
