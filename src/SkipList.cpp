#include "SkipList.h"

// Destructor
SkipList::~SkipList(){
    SkipNode* tmp=m_head->next()[0];
    while(tmp!=m_tail){
        SkipNode* toDelete=tmp;
        tmp=tmp->next()[0];
        delete toDelete;
    }
    delete m_tail;
    delete m_head;
};

// initializes the List
int SkipList::init(int maxLevel, int maxValue){
    
    m_maxLevel=maxLevel;
    m_maxValue=maxValue;
    
    m_head=new SkipNode(m_maxLevel);
    if (m_head)
        m_tail=new SkipNode(m_maxLevel, m_maxValue, m_head);
    if (m_head && m_tail)
        return 0;
    return -1;
}

// finds a node
const SkipNode* SkipList::find(int searchKey) const{
    SkipNode *tmp=m_head;
 
    int level=(m_maxLevel-1);
    while(level>=0){ 
        while(tmp->next()[level]!=m_tail && tmp->next()[level]->searchKey()<=searchKey){
            tmp=tmp->next()[level];
            if (tmp->searchKey()==searchKey)
                return tmp;
        }
        level--;
    }
    return NULL;
}

// inserts a new node with an allocated record
int SkipList::insert(int searchKey, const char* lastname, const char* firstname, float gpa ,
    int numOfCourses, const char* dependent, int postcode, int new_level)
{

    SkipNode *tmp=m_head;

    // create temporary nexts table
    SkipNode **prev=new SkipNode*[m_maxLevel];
    for (int i=0; i<m_maxLevel; i++){
        prev[i]=m_head;
    }
    int level=(m_maxLevel-1);
    while(level>=0){
        while(tmp->next()[level]!=m_tail && tmp->next()[level]->searchKey()<searchKey){
            tmp=tmp->next()[level];
            for (int i=level;i>=0;i--) prev[i]=tmp;
        }
        level--;
    }
    // build record
    Record* record = new Record(searchKey, lastname, firstname, gpa , numOfCourses, dependent, postcode);
    // build record
    SkipNode *newNode=new SkipNode(searchKey, record, new_level);

    // point new's nexts to previous' nexts, and previous' nexts to the new
    for (int level=0; level<newNode->level(); level++){
        newNode->next()[level]=prev[level]->next()[level];
        prev[level]->next()[level]=newNode;
    }

    // delete temporary table
    delete[] prev;  
    
    return 0;
}

// deletes a node
int SkipList::deleteNode(int searchKey){

    SkipNode *tmp=m_head;
    
    // create temporary nexts table
    SkipNode **prev=new SkipNode*[m_maxLevel];
    for (int i=0; i<m_maxLevel; i++){
        prev[i]=m_head;
    }

    int level=m_maxLevel-1;
    while(level>=0){
        while(tmp->next()[level]!=m_tail && tmp->next()[level]->searchKey()<searchKey){
            tmp=tmp->next()[level];
            for (int i=level;i>=0;i--) prev[i]=tmp;
        }
        level--;
    }
    
    SkipNode *toDelete=tmp->next()[0];
 
    if (toDelete->searchKey()!=searchKey)
        return -1; 
    
    for (int level=0; level<toDelete->level(); level++){
        prev[level]->next()[level]=prev[level]->next()[level]->next()[level];
    }

    // delete temporary table and the found node
    delete[] prev;
    delete toDelete;

    return 0;
}
