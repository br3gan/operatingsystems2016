// this needs to be changed to c++ 11 now
#include <iostream>
#include <time.h>
#include "SkipList.h"
#include <stdlib.h>

int main(int argc, char *argv[]){

    srand(time(NULL));
    //SkipNode* node1 = new SkipNode(4);
    //SkipNode* node2 = new SkipNode(4,NULL,10);
    //SkipNode* node3 = new SkipNode(4,10,node1);
    //delete node1;
    //delete node2;
    //delete node3;
   
     
    SkipList* list = new SkipList();
    
    list->init(4, 10);

    int rand_num;
    rand_num=rand()%4+1;
    list->insert(1, "Ntaveas1", "Stelios1", 6.95, 42, "DIT1", 15771, rand_num);
    std::cout << rand()%4+1 << std::endl;
    rand_num=rand()%4+1;
    list->insert(2, "Ntaveas2", "Stelios2", 6.96, 43,"DIT2", 15772, rand_num);
    std::cout << rand()%4+1 << std::endl;
    rand_num=rand()%4+1;
    list->insert(3, "Ntaveas3", "Stelios3", 6.97, 44,"DIT3", 15773, rand_num);
    std::cout << rand()%4+1 << std::endl;
    rand_num=rand()%4+1;
    list->insert(4, "Ntaveas4", "Stelios4", 6.97, 45,"DIT4", 15774, rand_num);
    std::cout << rand()%4+1 << std::endl;
    list->deleteNode(4);
    list->deleteNode(3);
    list->deleteNode(2);
    list->deleteNode(1);
    
    delete list;
    
    return 0;
}
