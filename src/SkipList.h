#include <iostream>
#include "SkipNode.h"

class SkipList{

private:
    /**
    * @brief Lists stat 
    */
    SkipNode* m_head;
    /**
    * @brief Lists end 
    */
    SkipNode* m_tail;

    /**
    * @brief The value of the list's end
    */
    int m_maxValue;
    /**
    * @brief The value of the max allowed nexts
    */
    int m_maxLevel;
    /**
    * @brief The number of active Nodes
    */
    int m_length;

public:

    
    /**
    * @brief Constructor
    */
    SkipList(){}

    /**
    * @brief Destructor. Deletes every Node and its compinents 
    */
    ~SkipList();

    /**
    * @brief Initailizes the List 
    *
    * @param maxLevel The maxLevel of the Nodes
    * @param maxValue The value of the tail
    *
    * @return Returns 0 upon success, -1 upon failure (wrong allocation)
    */
    int init(int maxLevel, int maxValue);
    
    /**
    * @brief Finds a node with a certain ID
    *
    * @param studetID The Node primary key
    *
    * @return Rerurns the Node class upon success, null upon failure
    */
    const SkipNode* find(int searchKey) const;
    
    /**
    * @brief Creates and inserts a new Node
    *
    * @param toInsertStudentID the primary key of the new Node
    *
    * @return returns 0 upon success, -1 upon failure (node already existed)
    */
    int insert(int searchKey, const char* lastname, const char* firstname,
        float gpa , int numOfCourses, const char* dependent, int postcode, 
        int new_level
    );
    
    /**
    * @brief Deletes a Node
    *
    * @param toDeleteStudentID The primary key of the node we are going to delete
    *
    * @return 0 upon success, -1 upon failure (node does not exist)
    */
    int deleteNode(int toDeleteSearchKey);

    /**
    * @brief Deletes whole SkipList
    *
    * @return 0 upon success
    */
    int deleteList();
};
